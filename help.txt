General Notes
document.cookie = "mycookiename=value; expires=some gmt date time; path=thepath; domain=the domain";
document.cookie = "mtracker=somevalue; expires=0; path=/";


*******************************************************************************************************
Usage

Create session cookie:

$.cookie('name', 'value');
Create expiring cookie, 7 days from then:

$.cookie('name', 'value', { expires: 7 });
Create expiring cookie, valid across entire site:

$.cookie('name', 'value', { expires: 7, path: '/' });
Read cookie:

$.cookie('name'); // => "value"
$.cookie('nothing'); // => undefined
Read all available cookies:

$.cookie(); // => { "name": "value" }
Delete cookie:

// Returns true when cookie was successfully deleted, otherwise false
$.removeCookie('name'); // => true
$.removeCookie('nothing'); // => false

// Need to use the same attributes (path, domain) as what the cookie was written with
$.cookie('name', 'value', { path: '/' });
// This won't work!
$.removeCookie('name'); // => false
// This will work!
$.removeCookie('name', { path: '/' }); // => true
Note: when deleting a cookie, you must pass the exact same path, domain and secure options that were used to set the cookie, unless you're relying on the default options that is.