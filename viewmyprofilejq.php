<?php
session_start();
if(!isset($_SESSION['login'])){
	header('location:mainjq.php');
	exit;
}
?>
<html>
<head>
<title>WEBOFFERS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="jquerymobile/jquery.js"></script>
<script src="jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
</head>
<script>
//My script goes here
	
$(document).ready(function(){

	$.ajax({			
                        type: 'POST',
                        url: 'viewmyprofilevalidation.php',
                        dataType: 'json',
                        success: function (data) {
                                console.log(data);
								var a=document.getElementsByClassName('viewus');
								var b=document.getElementsByClassName('viewfirst');	
								var c=document.getElementsByClassName('viewlast');	
								var d=document.getElementsByClassName('viewemail');
								var e=document.getElementsByClassName('viewtel');
								var f=document.getElementsByClassName('viewpass');
								a[0].value=data.username;
								b[0].value=data.firstname;
								c[0].value=data.lastname;
								d[0].value=data.email;
								e[0].value=data.tel;
								f[0].value=data.password;
						}
				});	
	$('#updatemyprofilebtn').click(function(){
			window.location='updatemyprofilejq.php';
		});
	$('.gohome').click(function(){ 
			 window.location='./mainjq.php';
		 });				
});
</script>
<body>
<div data-role='page' id='page-viewmyprofile'>
<div data-role='header'>
<h1>My Profile</h1>
<a href='#' id='updatemyprofilebtn' class='ui-btn ui-btn-right ui-shadow ui-corner-all ui-btn-icon-left ui-icon-edit'>Edit Profile</a>
	<div data-role='navbar' data-iconpos='left'>
			<ul>
				<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
					<?php
	
	if(isset($_SESSION['login'])){
		echo "<li><a href='#' data-icon='shop' id='cart' >Cart <span class='ui-li-count'>0</span></a></li>";
	}
	?>
					</ul>
		
		
		</div>
</div>

<div data-role='main' class='ui-content' id='viewprof'>
							<label for='viewus'>Username</label>
							<input disabled='disabled' type='text' value='' id='viewus' class='viewus'>
							<label for='viewfirst'>First name</label>
							<input disabled='disabled' type='text' value='' id='viewfirst' class='viewfirst'>
							<label for='viewlast'>Last name</label>
							<input disabled='disabled' type='text' value='' id='viewlast' class='viewlast'>
							<label for='viewemail'>Email</label>
							<input disabled='disabled' type='text' value='' id='viewemail' class='viewemail'>
							<label for='viewtel'>Telephone</label>
							<input disabled='disabled' type='text' value='' id='viewtel' class='viewtel'>
							<label for='viewpass'>Passowrd</label>
							<input disabled='disabled' type='text' value='' id='viewpass' class='viewpass'>
</div>
</div>

</body>
</html>