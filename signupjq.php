<?php
session_start();
?>
<html>
<head>
<title>HZ Shopping Site</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="jquerymobile/jquery.js"></script>
<script src="jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script>
$(document).ready(function(){

		 $('#signupform').submit(function(e) {
                e.preventDefault();
                $.ajax({
                        type: 'POST',
                        url: 'functions/signup_fn.php',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (data) {
                                console.log(data);
								if(data.success==true){
									window.location='mainjq.php';
								}else{
									$('#signup-msg').html(data.msg);
									$('#signup-msg').popup('open');
								}
						}
				});
		});

});


</script>
</head>
<body>
<div data-role="page" id="pagesignup"><!--OpenPage-->
 <div data-role="header">
 <h1>Signup</h1>
 </div><!--End of Header-->
 <div data-role="main" class="ui-content">
<form id='signupform'>
<label for="username">Username</label>
<input type='text' name='username' id="username" required>
<label for="email">Email</label>
<input type='email' name='email' id='email' required>
<label for="first">First name</label>
<input type='text' name='first' id='first' required>
<label for="last">Last name</label>
<input type='text' name='last' id='last' required>
<label for="tel">Telephone</label>
<input type='tel' name='telephone' id='tel' required>
<label for="pass">Password</label>
<input type='password' name='password' id='pass' required><br>
<input type='submit' name='submit' value='Signup' data-rel='popup'>
</form>
</div>
<div  data-role='footer'></div>
</div><!--Page Closure-->
<!--POPUP message-->
<div data-role="popup" id="signup-msg" data-theme="a" class="ui-corner-all"></div>
<!--END oF POPUP message-->
</body>
</html>