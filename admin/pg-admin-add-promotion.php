<?php
session_start();
	if(!isset($_SESSION['admin'])){
		header('location:../mainjq.php');
		exit;
	}
require_once('../functions/database.php');
$link=connect();
?>
<html>
<head>
<title>Admin/Add promotion</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css"/>
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script>
$(document).ready(function(){
	
	$('#form_add_promo').submit(function(event){
	event.preventDefault();	
	var formData = new FormData(this);
	$.ajax({
	type: 'POST',
	url : 'admin_add_promo_processor.php',
	data:formData,
            cache:false,
            contentType: false,
            processData: false,
			success:function(data){
                console.log("success");
                console.log(data);
            },
			error: function(data){
                console.log("error");
                console.log(data);
            }
			});
	});
	
	$('.gohome').click(function(){ 
			 window.location='../mainjq.php';
		 });
	$('.goAdmin').click(function(){ 
			 window.location='./Admin.php';
		 });

});
</script>
</head>
<body>
<div data-role="page" id="pg-admin-add-promotion">
	<div data-role='header'>
		<h2>Admin Panel</h2>
		<div data-role='navbar' data-iconpos='left'>
			<ul>
				<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
				<li><a href='#' data-icon='gear' class='goAdmin'>Admin Panel</a></li>
			</ul>
		</div>
	</div>
<div data-role='main' class='ui-content'><!--main-->
<!--Form-->
<form id="form_add_promo" enctype="multipart/form-data">

<!--Select Market-->
<div class="ui-field-contain">
<select id='select-market-list' name='promo_select_market' data-native-menu="false">
	<option disabled selected>Select Market</option>
<?php
	$req="SELECT * FROM markets ;";
	$res=mysqli_query($link,$req);
	while($tab1=mysqli_fetch_row($res)){		
		echo "<option value=$tab1[0]>$tab1[1]</option>";
	}
?>
</select>
</div>

<!--Select product category list-->
<div class="ui-field-contain">
<select id='select-cat-list' name='promo_select_cat' data-native-menu="false">
	<option disabled selected>Select Category</option>
<?php
	$req="SELECT * FROM category;";
	$res=mysqli_query($link,$req);
	$i=0;
	while($tab2=mysqli_fetch_row($res)){
		if($i==0){$i++;continue;}
		echo "<option value=$tab2[0]>$tab2[1]</option>";
	}
?>
</select>
</div>


<input type='text' name='promo_name' placeholder="Product name" required><br>
<input type='number' name='quantity' placeholder='Quantity' required><br> 
	<select id='select-quantity' name='unit' data-native-menu='false'>
	<option disabled selected>Quantity</option>
	<?php 
			$unit=['kg','g','pcs','L','ml','packet'];
			foreach($unit as $e)
			print("<option value='$e'>$e</option>");
		?>
		</select>
<input type='number' name='promo_price' placeholder="Price" required><br>
<input type='date' name='promo_start'><br>
<input type='date' name='promo_end'><br>
<input type='file' name='image' placeholder='Image'><br>
<input type="submit" id="ad" class="ui-btn ui-btn-inline" value="Add">

</form>
<div id="result"></div>
</div><!--End of main-->
</div>


</body>
</html>