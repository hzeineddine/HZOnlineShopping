<?php
session_start();
	if(!isset($_SESSION['admin'])){
		header('location:../mainjq.php');
		exit;
	}
?>
<html>
<head><title>Remove Branch</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css"/>
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
</head>
<script>
$(document).ready(function(){
$('#select').change(function(event){
	event.preventDefault();
	var id=document.getElementById('select').value;
	$('#select2').load('select_Location.php');
	$.ajax({
		type: 'GET',
		url: 'select_Location.php',
		data: {marketid:id},
success: function(data){
	console.log(data);
	$('#remove').html(data);
}
});	
});
$('#forme').submit(function(event){
	event.preventDefault();
$.ajax({
	type: 'GET',
	url: 'remove_branch.php',
	data: $(this).serialize(),
	success: function(data){
		console.log(data);
		$('#removed').html(data);
    	}
    });
  });
  
  $('.gohome').click(function(){ 
			 window.location='../mainjq.php';
		 });
	$('.goAdmin').click(function(){ 
			 window.location='./Admin.php';
		 });
		 
		 
});
</script>
<body>
<div data-role="page" id="pg6">
	<div data-role='header'>
		<h2>Admin Panel</h2>
		<div data-role='navbar' data-iconpos='left'>
			<ul>
				<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
				<li><a href='#' data-icon='gear' class='goAdmin'>Admin Panel</a></li>
			</ul>
		</div>
	</div>
<div data-role='main' class='ui-content'><!--main-->
<form id="forme">
<div class="ui-field-contain">
<select data-native-menu='false' id="select" name="mid">
<option>select a market</option>
<?php
$link=mysqli_connect("localhost","root","","project_db");
$request="SELECT mid,name FROM markets;";
$result=mysqli_query($link,$request);
while($tab=mysqli_fetch_row($result)){
echo "<option value='$tab[0]'>$tab[1]</option> ";
}
?>
</select>
</div>
<div id="select2"> </div>
<div id="remove"></div>
<input type='submit' value='Removebranch' class="ui-btn- ui-btn-inline"><br>
<div id="removed"></div>
</div><!--End of main-->
</div>
</form>
</body>
</html>