<?php
session_start();
	if(!isset($_SESSION['admin'])){
		header('location:../mainjq.php');
		exit;
	}
require_once('../functions/database.php');
$link=connect();
?>
<html>
<head>
<title>WEBOFFERS</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script>
$(document).ready(function(){
	
	$('#select_market_list').change(function(event){
		event.preventDefault();
		var market=document.getElementById('select_market_list').value;
		$('#select-field').load('remove_promo_list_processor.php #select_promo_to_delete_div ',{market:market});
	});

	$('#form_rm_promotion').submit(function(event){
    event.preventDefault();
    $.ajax({
    type: 'GET',
    url : 'remove.php',
    data: $(this).serialize(),
    success: function(data){
	console.log(data);
	$('#result1').html(data);
        }
		});
	});
	
	$('.gohome').click(function(){ 
			 window.location='../mainjq.php';
		 });
	$('.goAdmin').click(function(){ 
			 window.location='./Admin.php';
		 });
});
</script>
</head>
<body>

<div data-role="page" id="pg4">
<div data-role='header'>
		<h2>Admin Panel</h2>
		<div data-role='navbar' data-iconpos='left'>
			<ul>
				<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
				<li><a href='#' data-icon='gear' class='goAdmin'>Admin Panel</a></li>
			</ul>
		</div>
	</div>
<form id="form_rm_promotion">
<div class="ui-field-contain">
<select id='select_market_list' name='select_market_list' data-native-menu="false" required>
	<option disabled selected>Select a super market</option>
<?php
	$markets=array();
	$req="SELECT * FROM markets ;";
	$res=mysqli_query($link,$req);
	while($markets=mysqli_fetch_row($res)){		
		echo "<option value=$markets[0]>$markets[1]</option>";
	}
	
?>
</select>
</div>

<div  id='select-field'>
	
</div>
<input type='submit' value='Remove' class="ui-btn- ui-btn-inline" ><br>
<div id="result1"></div>
</div>
</form>
</body>
</html>