<?php
	session_start();
	if(!isset($_SESSION['login'])){
	header('location:mainjq.php');
	exit;
}
require_once('functions/database.php');
class UPDATEMYPROFILE{
	function update(){
		$return=array();
		$return['success']=false;
		$return['msg']='';
		$link=connect();
		extract($_POST);
		if( empty($email) || empty($password) || empty($tel) ){
			$return['success']=false;
			$return['msg']="<div style='padding:10px 20px;' id='update_msg'><h3>Please fill the form</h3></div>";
			return json_encode($return);
		}else{
			$request="UPDATE `clients` SET `password`='$password',`email`='$email',`tel`='$tel' WHERE username='$username';";
			if(mysqli_query($link,$request)){
				$return['success']=true;
			    $return['msg']="<div style='padding:10px 20px;' id='update_msg'><h3>Updated Successfully</h3></div>";
				return json_encode($return);
			}
		    else{
				$return['success']=false;
			    $return['msg']="<div style='padding:10px 20px;' id='update_msg'><h3>Error updating database !</h3></div>";
				return json_encode($return);
			}
		}
	}
}
$obj=new UPDATEMYPROFILE;
echo $obj->update();
