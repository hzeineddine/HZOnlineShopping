<?php
session_start();
require_once('functions/database.php');
$link=connect();
?>
<html>
<head>
<title>HZ Shopping Site</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="css/main.css"/>
<link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="jquerymobile/jquery.js"></script>
<script src="jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<!--Javascript-->
<script>

//Google Map
function initialize() {
 mapProp = {
    zoom:12,
	panControl:false,
    zoomControl:false,
    mapTypeControl:false,
    scaleControl:false,
    streetViewControl:false,
    overviewMapControl:false,
    rotateControl:true,    
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
  <?php //Automating locations loader
	 	  
	  //Loading map custom icons 
	  $query_icons="SELECT * FROM map_markers;";
	  $result_icons=mysqli_query($link,$query_icons);
	  $icons=array();
	  $tmp=array();
	  while($tmp=mysqli_fetch_row($result_icons)){
		  $icons[$tmp[0]]=$tmp[1];
	  }
	  //Loading locations
	  $tmp=array();
	  $query_markers="SELECT * FROM location;";	
	  $result_markers=mysqli_query($link,$query_markers);
	  if(mysqli_num_rows($result_markers)>0){
		  $map_markers=array();
		 while($tmp=mysqli_fetch_row($result_markers)){
			 /*
			 $tmp
			 0->lid
			 1->mid
			 2->address
			 3->lat
			 4->lng
			 */
			 $map_markers[$tmp[0]]=[$tmp[1],$tmp[3],$tmp[4]];
		 }		 
	  }
	  //Positioing markers
	  $tmp=array();
	  foreach($map_markers as $lid=>$tmp){
		  echo "var marker=new google.maps.Marker({
				position:new google.maps.LatLng($tmp[1],$tmp[2]),//Jnah
				icon:'icons/map_icons/$tmp[0].png',
				animation:google.maps.Animation.BOUNCE})
				marker.setMap(map);";
	  }
  ?>
  	
	//Getting my current position
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	
	
	map.setCenter(pos);
	var marker=new google.maps.Marker({
	position: pos,
	icon:'icons/map_icons/map-marker.png',
	animation:google.maps.Animation.BOUNCE
	})
	marker.setMap(map);

    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }
}//End of function initialize

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }
}
google.maps.event.addDomListener(window,'load', initialize);
//End of Google Map Script
//*******************************************************************************************************
if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var latitude=position.coords.latitude;
            var longitude=position.coords.longitude;
			$.ajax({
				type: 'GET',
				url: './Location/distance-engine.php',
				data: {lat:latitude,lng:longitude},
				dataType: 'json',
				success: function (data) {
					console.log(data);
					$('#market1').html(data.market1);
					$('#market2').html(data.market2);
					$('#market3').html(data.market3);
					$('.my_cards_holder').show();
				}	
		});
		});
}
//*******************************************************************************************************
$(document).ready(function(){
	$('.my_cards_holder').hide();

		if($.cookie('login')){
			$('.signed').show();	
			$('.not-signed').hide();
		}else{
			$('.signed').hide();
			$('.not-signed').show();
		}
		//Submitting Form -- Signing in
        $('#form1').submit(function(event) {
                event.preventDefault();
                $.ajax({
                        type: 'POST',
                        url: 'validate.php',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (data) {
                                console.log(data);
								if(data.login == false){
									$('#fail').html(data.dialog);
									$('#panelList').html(data.panel);
									
								}else{
									$.cookie('login','ok',{ expires: 7, path: '/' });
									$.cookie('userid',data.userid,{ expires: 7, path: '/' });
									$('#popupLogin').html(data.dialog);
									$('#panelList').html(data.panel);
									window.location.reload();
								}
						}
				});
		});
		//End of form submission
		
		$('#signoutpan').click(function(){		//Signing out
			 $.ajax({
                        type: 'GET',
                        url: 'signout.php',
                        success: function (data) {
                                console.log(data);
								$.removeCookie('login',{ expires: 7, path: '/' });
								$.removeCookie('userid',{ expires: 7, path: '/' });
								window.location.reload();
						}
				});
			
		});
		$('#viewmyprofilepan').click(function(){
			window.location='viewmyprofilejq.php';
		});
		$('#signuppan').click(function(){
			window.location='signupjq.php';
		});
		$('#adminpan').click(function(){
			window.location='admin/Admin.php';
		});
		
		

		$('#spinneys_href').click(function(){
			window.location='categories.php?mid=1';
		});
		$('#tsc_href').click(function(){
			window.location='categories.php?mid=2';
		});
		$('#carrefour_href').click(function(){
			window.location='categories.php?mid=3';
		});
		
		$('.gohome').click(function(){ 
			 window.location='./mainjq.php';
		 });
		$('#cartpan').click(function(){
			window.location='./cart/cart.php';
		});
		$('#viewallpan').click(function(){
			window.location='./all_markets.php';
		});
		$('#view_my_cart').click(function(){
			window.location='./cart/cart.php';
		});
});

						
</script>
</head>
<body
<div data-role="page" id="pageone" data-theme='a'>
<!--Panel-->
<div data-role="panel" id="myPanel">
<ul data-role="listview" id="panelList">
<li>
<a href='#popupLogin' data-rel='popup' data-position-to='window' class='not-signed ui-btn ui-shadow ui-icon-check ui-btn-icon-left ui-btn-a' data-transition='pop' id='signinpan'>Signin</a>
</li>
<li>
<a href='#' class='not-signed ui-btn ui-shadow  ui-icon-arrow-u ui-btn-icon-left ui-btn-a' id='signuppan'>Signup</a>
</li>
<?php if(isset($_SESSION['admin'])){
echo "<li><a href='#' class='ui-btn ui-shadow  ui-icon-gear ui-btn-icon-left ui-btn-a' id='adminpan'>Admin Panel</a></li";
}
?>
<li>
<a href='#' class='signed ui-btn ui-shadow  ui-icon-user ui-btn-icon-left ui-btn-a' id='viewmyprofilepan'>View my Profile</a>
</li>
<li>
<a href='#' class=' ui-btn ui-shadow  ui-icon-grid ui-btn-icon-left ui-btn-a' id='viewallpan'>View all markets</a>
</li>
<li>
<a href='#' class='signed ui-btn ui-shadow  ui-icon-shop ui-btn-icon-left ui-btn-a' id='cartpan' >My Cart</a>
</li>
<li>
<a href='#' class='signed ui-btn ui-shadow  ui-icon-minus ui-btn-icon-left ui-btn-a' id='signoutpan' >Signout</a>
</li>
</ul>
</div>
<!--End of Panel-->

<!--Header-->
<div data-role='header' data-position='fixed'>
<a href="#myPanel" class="ui-btn ui-icon-carat-r ui-shadow ">View</a>
<h1>HZ Shopping Site</h1>
	<div data-role='navbar' data-iconpos='left'>
			<ul>
				<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
					<?php
	
	if(isset($_SESSION['login'])){
		$c=count($_SESSION['cart']);
		echo "<li><a href='#' data-icon='shop' id='view_my_cart' >Cart <span class='ui-li-count'>$c</span></a></li>";
	}
	?>
					</ul>
		
		
		</div>
</div>
<!--End of Header-->

<div data-role="main" class="ui-content">
<div id="googleMap"></div><br><br><br>
	
<div id='mapmsg'></div>
	
	<div id='markets_loader_buttons'>
	<a href="#" id="spinneys_href" class="ui-btn">Spinneys</a>
	<a href="#" id="tsc_href" class="ui-btn">Tsc</a>
	<a href="#" id="carrefour_href" class="ui-btn">Carrefoure</a>
	</div>
	
	<div class='my_cards_holder'>
		<div class='card' id='market1'></div>
			<div class='card' id='market2'></div>
				<div class='card' id='market3'></div>
	</div>				
	
</div><!--End of main-->

</div>
<!--End of Page-->

<!--Signin Dialog-->
<div data-role="popup" id="popupLogin" data-theme="a" class="ui-corner-all">
				<form id="form1">
					<div style="padding:10px 20px;">
						<h3>Please sign in</h3>
				        <label for="un" class="ui-hidden-accessible">Username:</label>
				        <input type="text" name="user" id="un" value="" placeholder="username" data-theme="a">
				        <label for="pw" class="ui-hidden-accessible">Password:</label>
				        <input type="password" name="pass" id="pw" value="" placeholder="password" data-theme="a">
				    	<input type="submit" name="submit" id="submit" value="Signin" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check">
						<div id="fail"></div>
					</div>
				</form>
</div>	
<!--End of Signin Dialog-->

</body>
</html>
