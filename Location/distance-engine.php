<?php
session_start();
require_once '../functions/database.php';
$link=connect();
class DistanceEngine{
	public $lat;
	public $lng;

	function __construct($latitude, $longitude){
        $this->lat = deg2rad($latitude);
        $this->lng = deg2rad($longitude);
    }	
	
	function test(){
		$return=array();
		$lat=$this->lat;
		$lng=$this->lng;
		$return['msg']= "latitude = ".$lat." longitude = ".$lng;
		return json_encode($return);
	}
	
	
	function coord_to_assoc(){
		$link=connect();
		$query="SELECT * FROM location;";
		$res=mysqli_query($link,$query);
		/*
		$tab
		0->lid
		1->mid
		2->address
		3->lat
		4->lng
		*/
		$loc=array();
		$tab=array();
		while($tab=mysqli_fetch_row($res)){
			$loc[$tab[0]]=new DistanceEngine($tab[3],$tab[4]);		
		}
		return $loc;
	}
	
	function getDistance(DistanceEngine $other){
		$radiusOfEarth = 6371000;
		$diffLatitude = $other->lat - $this->lat;
        $diffLongitude = $other->lng - $this->lng;
		$a = sin($diffLatitude / 2) * sin($diffLatitude / 2) +
            cos($this->lat) * cos($other->lat) *
            sin($diffLongitude / 2) * sin($diffLongitude / 2);
        $c = 2 * asin(sqrt($a));
        $distance = $radiusOfEarth * $c;
		return $distance;
	}
	
	function json_haidarz($return){
		return json_encode($return);
	}
	
}
//main
$myloc=new DistanceEngine(isset($_GET['lat'])?$_GET['lat']:33.827724,isset($_GET['lng'])?$_GET['lng']:35.521252,0);
$markets_loc=$myloc->coord_to_assoc();
$distance=array();//array to hold distances
//Compare each market in the $markets_loc array with my location
foreach($markets_loc as $lid=>$other){
	$distance[$lid]=$myloc->getDistance($other);
}
asort($distance);
$return=array();
$i=1;
foreach($distance as $lid=>$dist){
	if($i==4){break;}
		$query1="SELECT mid,address,lat,lng FROM location where lid=$lid;";
		$result1=mysqli_query($link,$query1);
		$tab1=mysqli_fetch_row($result1);
		$query1_1="SELECT name FROM markets where mid=$tab1[0];";
		$result1_1=mysqli_query($link,$query1_1);
		$tab1_1=mysqli_fetch_row($result1_1);
		$return["market$i"]="<h2>$tab1_1[0]</h2><h3>$tab1[1]</h3>";
		$i++;
	
}


echo $myloc->json_haidarz($return);


