<?php
session_start();
class ajaxValidate {

		function signin_check(){
			$return=array();
			$return['dialog']='';
			$return['login']='';
			$return['panel']='';
			$return['userid']='';
			$return['admin']=false;
			extract($_POST);
			if (!isset($user) || empty($user) || !isset($pass) || empty($pass)){
                    $return['login'] = false;
                    $return['dialog']='<h3>Invalid username or password</h3>';
					$return['panel']="<li>
					<a href='#popupLogin' data-rel='popup' data-position-to='window' class='ui-btn ui-shadow ui-btn-inline ui-icon-check ui-btn-icon-left ui-btn-a' data-transition='pop' id='signinpan'>Signin</a>
					</li>
					<li>
					<a href='signupjq.php' class='ui-btn ui-shadow ui-btn-inline ui-icon-arrow-u ui-btn-icon-left ui-btn-a' id='signuppan'>Signup</a>
					</li>";
					return json_encode($return);   
            }
			$user=trim($user);
			$pass=trim($pass);
			if($user=='admin' && $pass=='admin'){
					$_SESSION['admin']='ok';
					$return['admin']=true;
					$return['dialog']='<div style="padding:10px 20px;"><h2>Hello Admin!</h2></div>';
					$return['login']=true;
					$return['panel']="
				<li>
				<a href='admin/Admin.php' class='admin-signed ui-btn ui-shadow  ui-icon-arrow-u ui-btn-icon-left ui-btn-a' id='adminpan'>Admin Panel</a>
				</li>
				<li>
				<a href='viewmyprofilejq.php' class='signed ui-btn ui-shadow ui-icon-user ui-btn-icon-left ui-btn-a' id='viewmyprofilepan'>View my Profile</a>
				</li>
				<li>
				<a href='#' class='signed ui-btn ui-shadow ui-icon-minus ui-btn-icon-left ui-btn-a' id='signoutpan'>Signout</a>
				</li>";
				return json_encode($return);
			}
			$link=mysqli_connect("localhost","root","","project_db") or die("No connection !");		
			$query="SELECT cid,username,password FROM clients WHERE username='$user' and password='$pass';";
			$res=mysqli_query($link,$query);			
			if(mysqli_num_rows($res)==0){
				$return['dialog']='<h3>Invalid username or password</h3>';
				$return['login']=false;
				$return['panel']="
				<li>
				<a href='#popupLogin' data-rel='popup' data-position-to='window' class='not-signed ui-btn ui-shadow  ui-icon-check ui-btn-icon-left ui-btn-a' data-transition='pop' id='signinpan'>Signin</a>
				</li>
				<li>
				<a href='signupjq.php' class='not-signed ui-btn ui-shadow ui-icon-arrow-u ui-btn-icon-left ui-btn-a' id='signuppan'>Signup</a>
				</li>";
				return json_encode($return);
			}
			else{
					$_SESSION['login']='ok';
					$tab=mysqli_fetch_row($res);
					$_SESSION['userid']=$tab[0];
					$_SESSION['cart']=array();
					$query_cart="SELECT pid FROM shoppingcart WHERE cid=$tab[0];";
					$res_cart=mysqli_query($link,$query_cart);
					$tmp=array();
					while($tmp=mysqli_fetch_row($res_cart)){
						$_SESSION['cart'][]=$tmp[0];
					}
					//$return['userid']=$tab[0];
					$return['dialog']='<div style="padding:10px 20px;"><h2>Login Successfull!</h2></div>';
					$return['login']=true;
					$return['panel']="<li>
					<a href='viewmyprofilejq.php' class='signed ui-btn ui-shadow ui-icon-user ui-btn-icon-left ui-btn-a' id='viewmyprofilepan'>View my Profile</a>
					</li>
					<li>
					<a href='#' class='signed ui-btn ui-shadow ui-icon-minus ui-btn-icon-left ui-btn-a' id='signoutpan'>Signout</a>
					</li>";
					return json_encode($return);
				}
		}
}

$ajaxValidate = new ajaxValidate;
echo $ajaxValidate->signin_check();
